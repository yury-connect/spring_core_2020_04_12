import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.project.animals.entities.*;
import ru.project.days.entities.WeekDay;


// https://javarush.ru/groups/posts/477-spring-dlja-lenivihkh-osnovih-bazovihe-koncepcii-i-primerih-s-kodom-chastjh-2
public class Main {

    public static void main(String[] args) {
        /*   context  будет искать свои бины по аннотациям в указанном пакете:
        1. можно просто укаать класс с конфигом ...(ConfigAnimals.class);
        2. можно перечислисть классы конфигураций через запятую ...(ConfigAnimals.class, ConfigWeekDays.class);
        3. можно просто указать пакет ...("ru.project.configs");
        4. либо через запятую перечислить пакеты ...("ru.project.configs1", "ru.project.configs2");
        5. либо название пакета, который общий для них всех(подпакетов)...("ru.project");
         */
        ApplicationContext context = new AnnotationConfigApplicationContext("ru.project");
        Cat cat = context.getBean(Cat.class);
        Dog dog = (Dog) context.getBean("dog");
        Parrot parrot = context.getBean("parrot-kesha", Parrot.class);
        System.out.println(cat.getName() + "\n" + dog.getName() + "\n" + parrot.getName());

        /*
        Мы хотим создать бины наших животных при помощи автоматического сканирования пакета entities спрингом,
        а вот бин с днем недели создавать так, как описано в ConfigWeekDays (ru.project.configs).
        Все что надо сделать — это добавить на уровне класса ConfigWeekDays, который мы указываем
        при создании контекста в main-е аннотацию @ComponentScan, и указать в скобочках пакет,
        который надо просканировать и создать бины нужных классов  автоматически:
         */
        WeekDay weekDay = context.getBean(WeekDay.class);
        System.out.println("\n\n\tIt's " + weekDay.getWeekDayName() + " today!");

        /*
        Получается, что при создании контекста спринг видит, что ему нужно обработать класс ConfigWeekDays.
        Заходит в него и видит, что нужно просканировать пакет "ru.project.days.entities"
        и создать бины тех класов, после чего выполняет метод getDay() из класса ConfigWeekDays
        и добавляет бин типа WeekDay себе в контекст. В методе main() мы теперь имеем доступ
        ко всем нужным нам бинам: и к объектам животных, и к бину с днем недели.
         */
    }
}

/*
Резюме:
- стараться использовать автоматическую конфигурацию;
- при автоматической конфигурации указываем имя пакета, где лежат классы, бины которых надо создать;
- такие классы помечаются аннотацией @Component;
- спринг проходит по всем таким классам и создает их объекты и помещает себе в контекст;
- если автоматическая конфиграция нам по каким-то причинам не подходит — используем java-конфигурирование;
- в таком случае создаем обычный джава класс, методы которого будут возвращать нужные нам объекты,
    и помечаем такой класс аннотацией @Configuration на случай, если будем сканировать весь пакет целиком,
    а не указывать конкретный класс с конфигурацией при создании контекста;
- методы этого класса, которые возвращают бины — помечаем аннотацией @Bean;
    если хотим подключить возможность автоматического сканирования
    при использовании java-конфигурации — используем аннотацию @ComponentScan.
 */
