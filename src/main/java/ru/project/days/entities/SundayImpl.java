package ru.project.days.entities;


public class SundayImpl implements WeekDay {

    @Override
    public String getWeekDayName() {
        return "Sunday";
    }
}
