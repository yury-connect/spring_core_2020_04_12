package ru.project.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.project.days.entities.*;
import java.time.DayOfWeek;
import java.time.LocalDate;


/*
Стоит задача при запуске нашего приложения поместить в контекст такой бин,
который бы соответствовал текущему дню недели. Не все бины всех классов,
которые имплементят WeekDay интерфейс, а только нужный нам. Это можно сделать примерно так:
 */
@Configuration
@ComponentScan("ru.project.days.entities")public class ConfigWeekDays {
    @Bean
    public WeekDay getDay() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        switch (dayOfWeek) {
            case MONDAY: return new MondayImpl();
            case TUESDAY: return new TuesdayImpl();
            case WEDNESDAY: return new WednesdayImpl();
            case THURSDAY: return new ThursdayImpl();
            case FRIDAY: return new FridayImpl();
            case SATURDAY: return new SaturdayImpl();
            default: return new SundayImpl();
        }
    }
}
