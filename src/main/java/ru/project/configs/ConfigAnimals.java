package ru.project.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.project.animals.entities.*;


@Configuration
public class ConfigAnimals {

    @Bean
    public Cat getCat(Parrot parrot) {
        Cat cat = new Cat();
        cat.setName(parrot.getName() + " -killer" ); // dependency injection: сначала создастся bean "parrot"
        return cat;
    }
//    public Cat getCat() {
//        return new Cat();
//    }

    @Bean("dog")
    public Dog getDog() {
        return new Dog("name: gav-gav");
    }

    @Bean("parrot-kesha")
    public Parrot weNeedMoreParrots() {
        return new Parrot("name: parrot-kesha");
    }
}
